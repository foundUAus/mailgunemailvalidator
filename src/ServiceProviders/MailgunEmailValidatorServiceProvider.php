<?php

namespace foundU\MailgunEmailValidator\ServiceProviders;

use foundU\MailgunEmailValidator\Validator\MailgunEmailValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class MailgunEmailValidatorServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        Validator::extend('mailgun_validator', MailgunEmailValidator::class);
    }
}