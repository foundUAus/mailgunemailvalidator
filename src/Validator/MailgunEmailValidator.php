<?php

namespace foundU\MailgunEmailValidator\Validator;

use foundU\MailgunEmailValidator\MailgunEmailValidatorHelper;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;

class MailgunEmailValidator implements Rule
{
    /**
     * Reasons mailgun will return an email is not valid
     * There are some keys commented out for reference and if they need to be used down the track
     * If the commented keys are returned, a generic message will be rendered for it
     *
     * @var string[] $reasons
     */
    protected $reasons = [
        'no_mx / No MX host found' => 'The email provider may not receive emails due to invalid configuration',
        'no_mx' => 'The email provider may not receive emails due to invalid configuration',
        'high_risk_domain' => 'The email address belongs to a high risk domain',
        'tld_risk' => 'The email address belongs to a high risk domain',
        'mailbox_does_not_exist' => 'The email address does not exist',
        'mailbox_is_disposable_address' => 'The email address is disposable. Please use a different email',
        'long_term_disposable' => 'The email address is disposable. Please use a different email',
        // Following can be returned but at the moment, a generic message will be used instead
        //'unknown_provider' => '',
        //'No MX records found for domain' => '',
        //'subdomain_mailer' => '',
        //'immature_domain' => '',
        //'mailbox_is_role_address' => '',
        //'catch_all' => '',
        //'smtp_timeout' => '',
        //'smtp_error' => '',
    ];

    /**
     * @var string $genericEmailMessage
     */
    protected $genericEmailMessage = 'Unable to determine validity of email address';

    protected $message = '';

    /**
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = MailgunEmailValidatorHelper::validate($value);
        if (!is_array(($result))) {
            return true;
        }
        /*\Log::info('Invalid Email Address: ' . $value, [
            'reasons' => $result,
        ]);*/
        $this->message = $this->generateValidationMessageForUser($result);
        return false;
    }

    /**
     * @return array|string
     */
    public function message()
    {
        return $this->message;
    }

    /**
     * Use messages from Mailgun API and render a message for the user
     *
     * @param string[] $result
     * @return string
     */
    public function generateValidationMessageForUser(array $result): string
    {
        $message = [];
        $genericMessage = false;
        foreach ($result as $value) {
            // Reason doesnt exist in defined array so show a generic message
            if (!Arr::get($this->getReasons(), $value)) {
                $genericMessage = true;
                continue;
            }
            // Key found so use the defined message
            $message[] = Arr::get($this->getReasons(), $value);
        }

        // Didnt find a reason so a generic message will be appended
        if ($genericMessage) {
            $message[] = $this->getGenericEmailMessage();
        }

        return implode(' .', $message);
    }

    /**
     * @return string[]
     */
    public function getReasons(): array
    {
        return $this->reasons;
    }

    /**
     * @return string
     */
    public function getGenericEmailMessage(): string
    {
        return $this->genericEmailMessage;
    }
}