<?php

namespace foundU\MailgunEmailValidator;

use Illuminate\Support\Arr;

class MailgunEmailValidatorHelper
{
    /**
     * @param string $email
     * @return array|\ArrayAccess|bool|mixed
     */
    public static function validate(string $email)
    {
        // If credentials are not setup, return true and check other validation rules defined (if any)
        if (!env('MAILGUN_VALIDATOR_SECRET', env('MAILGUN_SECRET'))) {
            return true;
        }
        $mailgun = new MailGunClient();

        $result = $mailgun->post(
            '/v4/address/private/validate',
            [
                'address' => $email,
                'provider_lookup' => 'true'
            ]
        );

        $isDisposable = Arr::get($result, 'is_disposable_address', false) == true;
        $risk = Arr::get($result, 'risk');
        $deliverResult = Arr::get($result, 'result');
        $reason = Arr::get($result, 'reason');

        if (
            !$isDisposable &&
            $deliverResult == 'deliverable' &&
            in_array($risk, ['low', 'medium'])
        ) {
            /*if (!empty($reason)) {
                \Log::info(
                    'Passing validation but has a warning: ' . $email,
                    [
                        'reason' => json_encode($reason),
                    ]
                );
            }*/

            return true;
        }

        return $reason;
    }
}