<?php

namespace foundU\MailgunEmailValidator;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class MailGunClient
{
    /**
     * MailGunClient constructor.
     */
    public function __construct()
    {
        //
    }

    /**
     * @return string
     */
    protected function getBaseUri(): string
    {
        return 'https://api.mailgun.net';
    }

    /**
     * @return null|string
     */
    protected function getApiKey(): ?string
    {
        return env('MAILGUN_SECRET');
    }

    /**
     * Makes an authenticated api request
     *
     * @param $uri
     *
     * @return array
     * @throws \Exception
     */
    public function get($uri): array
    {
        $settings = [
            'base_uri' => $this->getBaseUri(),
            'timeout' => 15,
            'headers' => [
                'Accept' => 'application/json'
            ]
        ];

        try {
            $client = new Client($settings);
            $response = $client->get($uri, [
                'auth' => [
                    'api',
                    $this->getApiKey(),
                ]
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            \Log::warning(
                'Failed to run mailgun validation. ' . $e->getMessage(),
                [
                    'uri' => $uri,
                    'code' => $e->getCode(),
                    'response' => $e->getResponse()->getBody()->getContents(),
                ]
            );
            // Same response structure as mailgun api
            return [
                'reason' => [
                    'unable_to_connect'
                ],
            ];
        }
    }

    /**
     * Makes an authenticated post request
     *
     * @param $uri
     * @param $formParams
     *
     * @param int $timeout
     * @param int $connectTimeout
     * @return array
     */
    public function post($uri, $formParams, $timeout = 15, $connectTimeout = 15): array
    {
        $settings = [
            // Base URI is used with relative requests
            'base_uri' => $this->getBaseUri(),
            // You can set any number of default request options.
            'timeout' => $timeout,
            'connect_timeout' => $connectTimeout,
            'headers' => [
                'Accept' => 'application/json'
            ]
        ];

        try {
            $client = new Client($settings);
            $response = $client->post($uri, [
                'auth' => [
                    'api',
                    $this->getApiKey(),
                ],
                'form_params' => $formParams,
            ]);
            return json_decode($response->getBody()->getContents(), true);
        } catch (ClientException $e) {
            \Log::warning(
                'Failed to run mailgun validation. ' . $e->getMessage(),
                [
                    'uri' => $uri,
                    'code' => $e->getCode(),
                    'response' => $e->getResponse()->getBody()->getContents(),
                ]
            );
            // Same response structure as mailgun api
            return [
                'reason' => [
                    'unable_to_connect'
                ],
            ];
        }
    }
}