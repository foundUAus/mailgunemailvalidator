# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Installation

You can install the package via composer:

```bash
composer require foundu/mailgun-email-validator
```

## Usage

To use within a script, call the validate method from the helper class

``` php
$emailValid = foundU\MailgunEmailValidatorHelper::validate($email); 
```

To use with request validation or with the validator facade, simply add `mailgun_validator` to the rules of the email
attribute and validation will trigger automatically.

### How do I get set up? ###

You will require a Mailgun account to use this package. Once you have an account, retrieve the secret from the API
section of your dashboard. If you already use mailgun and have `MAILGUN_SECRET` already defined in your env, it will use
that.

However, if you are required to use a different secret from your main one, you can add `MAILGUN_VALIDATOR_SECRET` and
this will be detected first before checking for the regular secret variable.

* Dependencies
    - Mailgun Account with Validation Quota (PAYG if none)
    - Laravel
    - Guzzle

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact